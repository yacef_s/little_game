#include "include.h"
#include "fenetre.h"


int main()
{
    // Initialisation de la SDL
    SDL_Init(SDL_INIT_VIDEO);
    //TTF_Init();
    // Icone du jeu
    SDL_WM_SetIcon(SDL_LoadBMP("runner-icon.bmp"), NULL);

    fenetre();

    //TTF_Quit();
    SDL_Quit();
    return EXIT_SUCCESS;
}
