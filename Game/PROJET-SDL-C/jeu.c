#include "include.h"
#include "constantes.h"
#include "jeu.h"
#include "deplacerjoueur.h"
#include "fenetre.h"
// Donner une certaine position à l'image
SDL_Rect position, positionJoueur;

void jouer(SDL_Surface* ecran)
{
    SDL_Surface *link[4] = {NULL};
    // surface du link actuel
    SDL_Surface *linkActuel = NULL;
    SDL_Surface *obstacle = NULL;
    //Création des murs
    //SDL_Surface *mur =NULL;
    SDL_Event event;
    int continuer = 1;
    int i = 0, j =0;
    int carte[11][26];

    link[BAS] = SDL_LoadBMP("joueur.bmp");
    obstacle = SDL_LoadBMP("obstacle.bmp");
    //link[BAS] = SDL_LoadBMP("joueur.bmp");
    linkActuel = link[BAS];
    positionJoueur.x = 21;
    positionJoueur.y = 3;
    placement_ennemie_aleatoire(carte);
    // Repetition des touches
    SDL_EnableKeyRepeat(100,100);

    while(continuer){
        SDL_WaitEvent(&event);
        switch(event.type){
        case SDL_QUIT:
            continuer = 0;
            break;
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym){
            case SDLK_ESCAPE:
                continuer = 0;
                break;
            case SDLK_UP:
                deplacerJoueur(carte, &positionJoueur, HAUT);
                break;
            case SDLK_DOWN:
                deplacerJoueur(carte, &positionJoueur, BAS);
                break;
            case SDLK_RIGHT:
                deplacerJoueur(carte, &positionJoueur, DROITE);
                break;
            case SDLK_LEFT:
                deplacerJoueur(carte, &positionJoueur, GAUCHE);
                break;
            case SDLK_a:
                deplacerJoueur(carte, &positionJoueur, AGAUCHE);
                break;
            case SDLK_z:
                deplacerJoueur(carte, &positionJoueur, ADROITE);
                break;
            case SDLK_p:
                pause();
                break;
            default:
                break;
            }
        default:
            break;
        }
        SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0,0,0));
        // deplacement du joueur
        //position.x =  positionJoueur.x*TAILLE_BLOC;
        //position.y =  positionJoueur.y*TAILLE_BLOC;
        //SDL_BlitSurface(linkActuel,NULL,ecran, &position);
        for(i = 0; i < 10; i++){
            for(j = 0; j < 21; j++){
                 position.x =  i*TAILLE_BLOC;
                 position.y =  j*TAILLE_BLOC;
                switch(carte[i][j]){
                case OBSTACLE:
                    SDL_BlitSurface(obstacle,NULL,ecran, &position);
                    position.x--;
                    position.y--;
                    break;
                default:
                    break;
                }
            }
        }
        
        //SDL_BlitSurface(linkActuel,NULL,ecran, &position);
        position.x =  positionJoueur.x*TAILLE_BLOC;
        position.y =  positionJoueur.y*TAILLE_BLOC;
        SDL_BlitSurface(linkActuel,NULL,ecran, &position);

        SDL_Flip(ecran);
    }

    // Arreter la repetition des touches
    SDL_EnableKeyRepeat(0,0);
    SDL_FreeSurface(obstacle);
    for (i = 0; i <= 4; i++) {
        SDL_FreeSurface(link[i]);
    }
}

void placement_ennemie_aleatoire(int carte[][26]){
    int a = 0, b =0;
    
    for(a = 0; a < 10; a++){
        for(b = 0; b < 21; b++){
            if(carte[0][0] && carte[a][b] != LINK){
                int v = rand()%9;
                if(v == 0){
                    carte[a][b] = OBSTACLE;
                }
            }
        }
    }
    for(a = 0; a < 10; a ++){
        for(b = 0; b < 21; b++){
            if(carte[a][b] == LINK){
                if(carte[a+1][b] == 3){
                    carte[a+1][b] = 0;
                }
                if(carte[a-1][b] == 3){
                    carte[a-1][b] = 0;
                }
                if(carte[a][b+1] == 3){
                    carte[a][b+1] = 0;
                }
                if(carte[a][b-1] == 3){
                    carte[a][b-1] = 0;
                }
                
                
            }
        }
    }
}
