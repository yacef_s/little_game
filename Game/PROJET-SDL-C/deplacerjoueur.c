#include "include.h"
#include "constantes.h"
#include "deplacerjoueur.h"
#include "fenetre.h"


void deplacerJoueur(int carte[][26], SDL_Rect *pos, int direction)
{
    //SDL_Event event;
    //SDL_Surface *texte = NULL;
    //TTF_Font *police = NULL;
    switch(direction) {
    case HAUT:
        if(pos->y-1 < 0) {
            break;
        }
         if(carte[pos->x][pos->y-1] == OBSTACLE) {
             fin();
            break;
        }
        pos->y--;
        break;
    case BAS:
        if(pos->y+1 > 10) {
            break;
        }
        if(carte[pos->x][pos->y+1] == OBSTACLE) {
            fin();
            break;
        }
        pos->y++;
        break;
    case GAUCHE:
        if(pos->x - 1 < 0) {
            fin();
            break;
        }
         if(carte[pos->x-1][pos->y] == OBSTACLE) {
             fin();
            break;
        }
        pos->x--;
        break;
    case AGAUCHE:
        if(pos->x - 4 < 0) {
            fin();
            break;
        }
        if(carte[pos->x-4][pos->y] == OBSTACLE) {
            fin();
            break;
        }
        pos->x -=4;
        break;

    case DROITE:
        if(pos->x + 1 > 21){
            fin();
            break;
        }
         if(carte[pos->x][pos->y+1] == OBSTACLE) {
             fin();
             break;
        }
        pos->x++;
        break;
    case ADROITE:
        if(pos->x + 4 > 21){
            fin();
            break;
        }
         if(carte[pos->x][pos->y+4] == OBSTACLE) {
             fin();
             break;
        }
        pos->x +=4;
        break;

    default:
        break;

    }
}

