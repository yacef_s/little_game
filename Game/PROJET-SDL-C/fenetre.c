#include "include.h"
#include "fenetre.h"
#include "jeu.h"


void fenetre()
{
    // Le pointeur qui va stocker la surface de l'ecran
    SDL_Surface *ecran = NULL; //*texte = NULL;
    //TTF_Font *police = NULL;
    // Le pointeur qui va stocker l'image de fond de l'ecran
    SDL_Surface *menu = NULL;
    // Le coordonné de l'image de fond
    SDL_Rect positionMenu;

    int continuer = 3;
    SDL_Event event;
    //SDL_Color couleurWhite = {255, 255, 255};

    positionMenu.x = 0;
    positionMenu.y = 0;
    // On tente d'ouvrir la fenetre
    ecran = SDL_SetVideoMode(762, 375, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    //police = TTF_OpenFont("angelina.ttf", 65);
    //texte = TTF_RenderText_Blended(police, "Press ECHAP TO BEGIN !", couleurWhite);
     if(ecran == NULL)
        {
            fprintf(stderr, "Impossible de charger le mode vidéo : %s\n", SDL_GetError());
            exit(EXIT_FAILURE);
        }
     // Nom du jeu
    SDL_WM_SetCaption("Endless RUNNER", NULL);
    // Chargement de l'image de fond
    menu = SDL_LoadBMP("menu.bmp");
     while (continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
        case SDL_QUIT:
                continuer = 0;
                break;
                // On va attendre une touche
        case SDL_KEYDOWN:
            // on va switcher entre les differentes lettres sur le clavier
            switch(event.key.keysym.sym)
                {
                    //si on appuye sur la touche ECHAPPE
                case SDLK_ESCAPE:
                    continuer = 0; // le jeu s'arrete
                    break;
                case SDLK_SPACE:
                    jouer(ecran);
                    break;
                default:
                    break;
                }
            break;
        default:
            break;
        }
        //  On raffraichi l'ecran à chaque fois que la boucle while tourne
        //  On blitte par-dessus l'écran
        SDL_BlitSurface(menu, NULL, ecran, &positionMenu);
        //positionMenu.x = 5;
        //positionMenu.y = 5;
        //SDL_BlitSurface(texte, NULL, ecran, &positionMenu);
        // le raffraichissemnt de l'écran
        SDL_Flip(ecran);
    }
     //SDL_FreeSurface(texte);
    SDL_FreeSurface(menu);
}

void pause()
{
    // Le pointeur qui va stocker la surface de l'ecran
    SDL_Surface *ecran = NULL; //*texte = NULL;
    //TTF_Font *police = NULL;
    // Le pointeur qui va stocker l'image de fond de l'ecran
    SDL_Surface *menu = NULL;
    // Le coordonné de l'image de fond
    SDL_Rect positionMenu;

    //int continuer = 3;
    SDL_Event event;
    //SDL_Color couleurWhite = {255, 255, 255};

    positionMenu.x = 0;
    positionMenu.y = 0;
    // On tente d'ouvrir la fenetre
    ecran = SDL_SetVideoMode(480, 360, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    //police = TTF_OpenFont("angelina.ttf", 65);
    //texte = TTF_RenderText_Blended(police, "Press ECHAP TO BEGIN !", couleurWhite);
     if(ecran == NULL)
        {
            fprintf(stderr, "Impossible de charger le mode vidéo : %s\n", SDL_GetError());
            exit(EXIT_FAILURE);
        }
     // Nom du jeu
    SDL_WM_SetCaption("PAUSE", NULL);
    // Chargement de l'image de fond
    menu = SDL_LoadBMP("menu.bmp");

     while (1)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
        case SDL_QUIT:
            //continuer = 0;
            exit(0);
            break;
        case SDL_KEYDOWN:
            // on va switcher entre les differentes lettres sur le clavier
            switch(event.key.keysym.sym)
                {
                    //si on appuye sur la touche ECHAPPE
                case SDLK_SPACE:
                    ecran = SDL_SetVideoMode(762, 375, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
                    jouer(ecran); // le jeu s'arrete
                    break;
                default:
                    break;
                }
            break;

        default:
            break;
        }

    }    
    //  On raffraichi l'ecran à chaque fois que la boucle while tourne
    //  On blitte par-dessus l'écran
    SDL_BlitSurface(menu, NULL, ecran, &positionMenu);
    //positionMenu.x = 5;
    //positionMenu.y = 5;
    //SDL_BlitSurface(texte, NULL, ecran, &positionMenu);
    // le raffraichissemnt de l'écran
    SDL_Flip(ecran);
//SDL_FreeSurface(texte);
    SDL_FreeSurface(menu);
 
}



void fin()
{
    // Le pointeur qui va stocker la surface de l'ecran
    SDL_Surface *ecran = NULL; //*texte = NULL;
    //TTF_Font *police = NULL;
    // Le pointeur qui va stocker l'image de fond de l'ecran
    SDL_Surface *menu = NULL;
    // Le coordonné de l'image de fond
    SDL_Rect positionMenu;

    //int continuer = 3;
    //SDL_Event event;
    //SDL_Color couleurWhite = {255, 255, 255};

    positionMenu.x = 0;
    positionMenu.y = 0;
    // On tente d'ouvrir la fenetre
    ecran = SDL_SetVideoMode(480, 360, 32, SDL_HWSURFACE | SDL_DOUBLEBUF);
    //police = TTF_OpenFont("angelina.ttf", 65);
    //texte = TTF_RenderText_Blended(police, "Press ECHAP TO BEGIN !", couleurWhite);
     if(ecran == NULL)
        {
            fprintf(stderr, "Impossible de charger le mode vidéo : %s\n", SDL_GetError());
            exit(EXIT_FAILURE);
        }
     // Nom du jeu
    SDL_WM_SetCaption("GAMEOVER", NULL);
    // Chargement de l'image de fond
    menu = SDL_LoadBMP("menu.bmp");
      //  On raffraichi l'ecran à chaque fois que la boucle while tourne
    //  On blitte par-dessus l'écran
    SDL_BlitSurface(menu, NULL, ecran, &positionMenu);
    //positionMenu.x = 5;
    //positionMenu.y = 5;
    //SDL_BlitSurface(texte, NULL, ecran, &positionMenu);
    // le raffraichissemnt de l'écran
    SDL_Flip(ecran);
//SDL_FreeSurface(texte);
    SDL_FreeSurface(menu);
}
