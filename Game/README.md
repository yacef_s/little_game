# Little Game


## Consignes

Vous commandez un vaisseau spatial intergalactique en mission pour la fédération des planètes du noyau et vous devez absolument atteindre le portail stellaire du quadrant pour délivrer un message de la plus haute importance au commandement de la troisième flotte près de la station profonde 9.

On compte sur vous !

... Terminé

Le but du projet et de faire un endless runner en 2D avec une interphace graphique. Pour cela devez utiliser la SDL2. Attention si le côté fonctionnel du projet reste plus important une attention toute particulière sera apportée à la qualité et la factorisation du code lors de la soutenance.

## Le projet 

Votre vaisseau devra pouvoir se déplacer de haut en bas.

Vous devez gérer les collisions, c'est-à-dire que votre vaisseau ne pourra pas sortir de l'écran.

Des obstacles ou des ennemis apparaîtront depuis la droite de l'écran. il faudra les éviter !

Si votre vaisseau se fait toucher par un obstacle ou un ennemi, vous pouvez soit afficher directement le Game Over soit lui faire juste perdre quelques points de vie pour pouvoir jouer plus longtemps. C'est à vous de voir !

La mort de votre vaisseau sera la condition de fin de la partie.
